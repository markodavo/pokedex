//
//  PokemonDetailVC.swift
//  Pokedex
//
//  Created by Mark Davidson on 8/9/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.

import UIKit

class PokemonDetailVC: UIViewController {

    @IBAction func backBtnPressed(_ sender: Any) {
            dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var evoLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var nextEvoImg: UIImageView!
    @IBOutlet weak var currentEvoImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var pokedexIdLbl: UILabel!
    @IBOutlet weak var defenseLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    var pokemon: Pokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLbl.text = pokemon.name.capitalized;
        let img = UIImage(named: "\(pokemon.pokedexId)")
        
        mainImg.image = img
        currentEvoImg.image = img
        pokedexIdLbl.text = "\(pokemon.pokedexId)"
        
        
        pokemon.downloadPokemonDetails {
            self.updateUI()
            
            //Whatever we write will only be called after the network call is completed
            
        }
            
            
        }

    func updateUI() {
        attackLbl.text = pokemon.attack
        defenseLbl.text = pokemon.defence
        heightLbl.text = pokemon.height
        weightLbl.text = pokemon.weight
        typeLbl.text = pokemon.type
        descriptionLbl.text = pokemon.descritpion
        
        if pokemon.nextEvoutionId == "" {
            evoLbl.text = "No Evoutions"
            nextEvoImg.isHidden = true
        } else {
            nextEvoImg.isHidden = false
            nextEvoImg.image = UIImage(named: pokemon.nextEvoutionId)
            
            let str = "Next Evoution: \(pokemon.nextEvotuionName) - Lvl: \(pokemon.nextEvoutionLevel)"
            evoLbl.text = str 
        }
        
        
    }
}
