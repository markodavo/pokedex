//  Constants.swift
//  Pokedex
//
//  Created by Mark Davidson on 10/9/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DowloadComplete  = () -> ()
